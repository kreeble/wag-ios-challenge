Wag iOS Challenge
Garam Song
=================
- Stack Overflow API page size has been set to 50
- Auto-Layout macros have been used (inside folder 'ESAutoLayout') to assist
  with laying out in code. Although they aren't necessary, they reduce code
  and improve readability immensely.
